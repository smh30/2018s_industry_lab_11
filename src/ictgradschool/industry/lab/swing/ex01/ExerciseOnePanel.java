package ictgradschool.industry.lab.swing.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does bmi calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {


    private JButton calculateBMIButton;
    private JButton calculateHealthyWeightButton;
    private JTextField height;
    private JTextField weight;
    private JTextField bmi;
    private JTextField maxWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);


        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        calculateBMIButton = new JButton("Calculate bmi");
        calculateHealthyWeightButton = new JButton("Calculate Healthy Weight");
        height = new JTextField(10);
        weight = new JTextField(10);
        bmi = new JTextField(10);
        maxWeight = new JTextField(10);


        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightLabel = new JLabel("Height in metres: ");
        JLabel weightLabel = new JLabel("Weight in kilograms: ");
        JLabel bmiLabel = new JLabel("Your Body Mass Index (bmi) is: ");
        JLabel maxWeightLabel = new JLabel("Maximum Healthy Weight for your Height: ");


        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        this.add(heightLabel);
        this.add(height);
        this.add(weightLabel);
        this.add(weight);
        this.add(calculateBMIButton);
        this.add(bmiLabel);
        this.add(bmi);
        this.add(calculateHealthyWeightButton);
        this.add(maxWeightLabel);
        this.add(maxWeight);


        calculateBMIButton.addActionListener(this);
        calculateHealthyWeightButton.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the bmi or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {


        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        if (event.getSource() == calculateBMIButton) {
            // calculate the BMI from the given height and weight
            // put the value back to the 'bmi' text field
            double myBMI = (Double.parseDouble(weight.getText())) / ((Double.parseDouble(height.getText())) * (Double.parseDouble(height.getText())));
            myBMI = roundTo2DecimalPlaces(myBMI);
            bmi.setText("" + myBMI);

        } else if (event.getSource() == calculateHealthyWeightButton) {
            //calculate the maxweight
            // put the value back to the 'max' text field
            double maxiWeight = 24.9 * (Double.parseDouble(height.getText())) * (Double.parseDouble(height.getText()));
            maxiWeight = roundTo2DecimalPlaces(maxiWeight);
            maxWeight.setText("" + maxiWeight);

        }


    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}