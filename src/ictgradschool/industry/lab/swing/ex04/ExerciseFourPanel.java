package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.DataInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static java.awt.event.KeyEvent.*;

/**
 * Displays an animated myBalloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private Balloon myBalloon;
    private JButton moveButton;
    private Timer timer;
    private ArrayList<Balloon> balloonList;
    private Timer increaseBalloons;
    private Timer detonate;
    private Timer floating;


    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        myBalloon = new Balloon(30, 60);

        balloonList = new ArrayList<>();
        //balloonList.add(0, new Balloon(150, 150));


        this.addKeyListener(this);
        this.timer = new Timer(200, this);

        this.increaseBalloons = new Timer(5000, this);
        increaseBalloons.start();

        detonate = new Timer(1500, this);

        this.floating = new Timer(200, this);

    }

    /**
     * Moves the myBalloon and calls repaint() to tell Swing we need to redraw ourselves.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // if the "event" was the increaseBalloons timer going off:
        if (e.getSource() == increaseBalloons) {
            Balloon extra = new Balloon((int) (Math.random() * 800), 800);
            extra.setDirection(Direction.Up);
            balloonList.add(extra);
            floating.start();

        // if the "event" was the detonation timer going off:
        } else if (e.getSource() == detonate) {
            for (int i = 0; i < balloonList.size(); i++) {
                if (balloonList.get(i).getBalloonColor() == Color.red) {
                    balloonList.remove(i);
                    i--;
                    repaint();
                }
            }

        //if it was the movement timer for the random balloons;
        } else if (e.getSource() == floating) {
            for (int i = 0; i < balloonList.size(); i++) {

                //balloonList.get(i).setDirection(balloonList.get(i).reverseDirection());
                balloonList.get(i).move();
                repaint();

            }
        }

        // if the "event" was something else ie the normal animation timer going off:
        else {
            myBalloon.move();
            // if it hits the edge it will go back
            for (int i = 0; i < 800; i++) {
                if (myBalloon.contains(new Point(0, i))) {
                    myBalloon.setDirection(Direction.Right);
                } else if (myBalloon.contains(new Point(i, 0))) {
                    myBalloon.setDirection(Direction.Down);
                } else if (myBalloon.contains(new Point(800, i))) {
                    myBalloon.setDirection(Direction.Left);
                } else if (myBalloon.contains(new Point(i, 800))) {
                    myBalloon.setDirection(Direction.Up);
                }
            }

            // events to happen whenever my balloon hits another balloon
            // for each of the random balloons
            for (int i = 0; i < balloonList.size(); i++) {
                // make an arraylist list of teh points of the random balloon
                ArrayList<Point> balloonPts = new ArrayList<Point>();
                Balloon cB = balloonList.get(i);
                for (int bx = cB.getX(); bx < (cB.getX() + cB.getSize() * 8); bx++) {
                    for (int by = cB.getY(); by < (cB.getY() + cB.getSize() * 9); by++) {
                        balloonPts.add(new Point(bx, by));
                    }
                }

                //if the any point of myballoon intersects with a random balloon
                for (int i1 = 0; i1 < balloonPts.size(); i1++) {
                    if (myBalloon.contains(balloonPts.get(i1))) {
                        //set random balloon's colour to red
                        cB.setColor(Color.red);
                        //reverse the direction of my balloon
                        myBalloon.setDirection(myBalloon.reverseDirection());
                        myBalloon.move();
                        //start a countdown to destroy the balloon i hit
                        detonate.start();

                    }
                }
            }
        }
        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();

    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == VK_S) {
            timer.stop();
        } else {
            timer.start();
            if (e.getKeyCode() == VK_UP) {
                myBalloon.setDirection(Direction.Up);
            } else if (e.getKeyCode() == VK_DOWN) {
                myBalloon.setDirection(Direction.Down);
            } else if (e.getKeyCode() == VK_LEFT) {
                myBalloon.setDirection(Direction.Left);
            } else if (e.getKeyCode() == VK_RIGHT) {
                myBalloon.setDirection(Direction.Right);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    /**
     * Draws any balloons we have inside this panel.
     *
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        myBalloon.draw(g);


        for (int i = 0; i < balloonList.size(); i++) {
            balloonList.get(i).draw(g);
        }


        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }
}