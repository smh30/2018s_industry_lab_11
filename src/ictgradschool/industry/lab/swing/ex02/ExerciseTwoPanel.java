package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {
    private JTextField num1;
    private JTextField num2;
    private JTextField result;
    private JButton add;
    private JButton subtract;

    /**
     * Creates a new ExerciseTwoPanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        num1 = new JTextField(10);
        num2 = new JTextField(10);
        result = new JTextField(20);
        add = new JButton("Add");
        subtract = new JButton("Subtract");
        JLabel resultLabel = new JLabel("Result: ");

        this.add(num1);
        this.add(num2);
        this.add(add);
        this.add(subtract);
        this.add(resultLabel);
        this.add(result);

        add.addActionListener(this);
        subtract.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        double n1 = Double.parseDouble(num1.getText());
        double n2 = Double.parseDouble(num2.getText());
        if (e.getSource() == subtract) {
            n2 = 0 - n2;
        }
        result.setText("" + roundTo2DecimalPlaces(n1 + n2));

// leaving this just in case but the one above looks nicer :)
//        if (e.getSource()==add){
//            double sum = Double.parseDouble(num1.getText()) + Double.parseDouble(num2.getText());
//            sum = roundTo2DecimalPlaces(sum);
//            result.setText("" + sum);
//
//        }else if(e.getSource()== subtract){
//            double minus =Double.parseDouble(num1.getText()) - Double.parseDouble(num2.getText());
//            minus = roundTo2DecimalPlaces(minus);
//            result.setText("" + minus);
//        }

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}